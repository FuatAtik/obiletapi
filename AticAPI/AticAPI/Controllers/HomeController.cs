﻿using AticAPI.BusinessLayer.Contracts;
using AticAPI.Entities.Entities.Core;
using AticAPI.Entities.Entities.Main;
using AticAPI.Models;
using AticAPI.Models.VM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Diagnostics;

namespace AticAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class HomeController : Controller
    {
        private IRepositoryWrapper _repositoryWrapper;

        public HomeController(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }


        public async Task<IActionResult> Index()
        {
            if (string.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "DeviceId")) && string.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "SessionId")))
            {
                var session = await _repositoryWrapper.Session.GetSession();

                CurrentSession.Set(HttpContext.Session, "DeviceId", session.Data.DeviceId);
                CurrentSession.Set(HttpContext.Session, "SessionId", session.Data.SessionId);
            }

            var busLocations = _repositoryWrapper.Location.GetBusLocations(
                new BusLocation()
                {
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Data = null,
                    Date = DateTime.Now,
                    Language = "tr-TR"
                });

            SelectList busLocationList = new SelectList(busLocations.Result.Data, "Id", "Name");

            BusLocationVM busLocationVM = new BusLocationVM()
            {
                selectList = busLocationList
            };

            return Ok(busLocationVM);
        }

        [HttpPost]
        public async Task<IActionResult> SearchTicket(int originId, int destinationId, DateTime date)
        {
            var journeyList = await _repositoryWrapper.Journey.GetBusJourneys(
                new BusJourney()
                {
                    Data = new Data()
                    {
                        OriginId = originId,
                        DestinationId = destinationId,
                        DepartureDate = date
                    },
                    Date = DateTime.Now,
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Language = "tr-TR"
                });

            BusJourneyVM busJourneyViewModel = new BusJourneyVM()
            {
                busJourneyDatas = journeyList.Data
            };
            return View(busJourneyViewModel);
        }
    }
}