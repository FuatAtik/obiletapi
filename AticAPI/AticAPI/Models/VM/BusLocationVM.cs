﻿using AticAPI.Entities.Entities.Main;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.Models.VM
{
    public class BusLocationVM
    {
        public SelectList selectList { get; set; }

        public DataBusLocation dataBusLocation { get; set; }
    }
}
