﻿using Newtonsoft.Json;

namespace AticAPI.Models
{
    public static class CurrentSession
    {
        public static void Set(this ISession session, string key, string value)
        {
            session.SetString(key, value);
        }

        public static string Get(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : value;
        }
    }
}
