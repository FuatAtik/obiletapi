﻿using AticAPI.Entities.Entities.Main;
using AticAPI.Entities.Entities.PostModel;

namespace AticAPI.BusinessLayer.Abstract
{
    public interface ILocationService
    {
        Task<BusLocationResponseData> GetBusLocations(BusLocation busLocation);
    }
}
