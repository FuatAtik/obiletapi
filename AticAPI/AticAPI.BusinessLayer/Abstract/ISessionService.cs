﻿using AticAPI.Entities.Entities.PostModel;

namespace AticAPI.BusinessLayer.Abstract
{
    public interface ISessionService
    {
        Task<SessionResponseModel> GetSession();
    }
}
