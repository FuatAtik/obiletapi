﻿namespace AticAPI.BusinessLayer.Abstract
{
    public interface IRestRequestService
    {
        Task<T> PostRequest<T>(object model, string methodUrl);
    }
}
