﻿using AticAPI.Entities.Entities.Main;
using AticAPI.Entities.Entities.PostModel;

namespace AticAPI.BusinessLayer.Abstract
{
    public interface IJourneyService
    {
        Task<BusJourneyResponseData> GetBusJourneys(BusJourney busJourney);
    }
}
