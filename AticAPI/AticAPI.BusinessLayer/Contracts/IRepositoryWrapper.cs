﻿using AticAPI.BusinessLayer.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.BusinessLayer.Contracts
{
    public interface IRepositoryWrapper
    {
        IJourneyService Journey { get; }
        ILocationService Location { get; }
        IRestRequestService RestRequest { get; }
        ISessionService Session { get; }
    }
}
