﻿using AticAPI.BusinessLayer.Abstract;
using AticAPI.BusinessLayer.Concrete;
using AticAPI.Entities.Entities.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.BusinessLayer.Contracts
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private readonly IRestRequestService _restRequest;
        private readonly ISessionService _session;
        private readonly BaseUrl _baseUrl;
        public RepositoryWrapper(IRestRequestService restRequest, BaseUrl baseUrl, ISessionService session)
        {
            _restRequest = restRequest;
            _baseUrl = baseUrl;
            _session = session;
        }

        private IJourneyService _journeyService;
        private ILocationService _locationService;
        private IRestRequestService _restRequestService;
        private ISessionService _sessionService;

        public IJourneyService Journey
        {
            get
            {
                if (_journeyService == null)
                {
                    _journeyService = new JourneyService(_restRequest);
                }
                return _journeyService;
            }
        }
        public ILocationService Location
        {
            get
            {
                if (_locationService == null)
                {
                    _locationService = new LocationService(_restRequest,_session);
                }
                return _locationService;
            }
        }
        public IRestRequestService RestRequest
        {
            get
            {
                if (_restRequestService == null)
                {
                    _restRequestService = new RestRequestService(_baseUrl);
                }
                return _restRequestService;
            }
        }
        public ISessionService Session
        {
            get
            {
                if (_sessionService == null)
                {
                    _sessionService = new SessionService(_restRequest);
                }
                return _sessionService;
            }
        }
       
    }
}
