﻿using AticAPI.BusinessLayer.Abstract;
using AticAPI.Entities.Entities.Core;
using AticAPI.Entities.Entities.Main;
using AticAPI.Entities.Entities.PostModel;
using Newtonsoft.Json;
using System.Net;
using System.Text;

namespace AticAPI.BusinessLayer.Concrete
{
    public class RestRequestService : IRestRequestService
    {
        private readonly BaseUrl _baseUrl;
        public RestRequestService(BaseUrl baseUrl)
        {
            _baseUrl = baseUrl;
        }

        public Task<T> PostRequest<T>(object model, string methodUrl)
        {

            using (var httpClient = new HttpClient())
            {
                httpClient.Timeout = new TimeSpan(0, 0, 360);

                httpClient.DefaultRequestHeaders.Add("Authorization", _baseUrl.Auth);

                StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");

                HttpResponseMessage responseData = httpClient.PostAsync(_baseUrl.Url + methodUrl, content).Result;

                string returnValue = responseData.Content.ReadAsStringAsync().Result;

                var response = JsonConvert.DeserializeObject<T>(returnValue);

                if (responseData.StatusCode != HttpStatusCode.OK)
                {
                    throw new InvalidOperationException("İşlem Başarısız Oldu!");

                }
                return Task.FromResult(response);
            }

        }
    }
}
