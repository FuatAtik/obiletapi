﻿using AticAPI.Entities.Entities.Core;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.Entities.Entities.Main
{
    public class BusJourney : BaseModelRequest
    {
        [JsonProperty("data")]
        public Data Data { get; set; }
    }
}
