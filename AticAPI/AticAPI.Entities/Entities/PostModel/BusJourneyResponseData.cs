﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AticAPI.Entities.Entities.Main;
using Newtonsoft.Json;

namespace AticAPI.Entities.Entities.PostModel
{
    public class BusJourneyResponseData : BaseModelResponse
    {
        [JsonProperty("data")]
        public List<BusJourneyData> Data { get; set; }
    }
}
