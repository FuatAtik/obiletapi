using Microsoft.Extensions.DependencyInjection;
using AticAPI.BusinessLayer.Contracts;
using AticAPI.BusinessLayer.Abstract;
using AticAPI.BusinessLayer.Concrete;
using AticAPI.Entities.Entities.Core;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

//builder.Services.AddCors(options=> options.AddDefaultPolicy(policy=>
//             policy.WithOrigins("http://localhost:7232/", "https://localhost:7232").AllowAnyHeader().AllowAnyMethod()
//         ));


builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromSeconds(10);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

builder.Services.AddScoped<ISessionService, SessionService>();
builder.Services.AddScoped<ILocationService, LocationService>();
builder.Services.AddScoped<IJourneyService, JourneyService>();
builder.Services.AddScoped<IRestRequestService, RestRequestService>();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.Configure<BaseUrl>(builder.Configuration.GetSection("BaseUrl"));
builder.Services.AddSingleton<BaseUrl>(bu =>
   bu.GetRequiredService<IOptions<BaseUrl>>().Value);

builder.Services.AddScoped<IRepositoryWrapper, RepositoryWrapper>();



var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
