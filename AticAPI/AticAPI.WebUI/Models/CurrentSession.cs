﻿using Microsoft.AspNetCore.Http;

namespace AticAPI.WebUI.Models
{
    public static class CurrentSession
    {
        public static void Set(ISession session, string key, string value)
        {
            session.SetString(key, value);
        }

        public static string? Get(ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default : value;
        }
    }
}
