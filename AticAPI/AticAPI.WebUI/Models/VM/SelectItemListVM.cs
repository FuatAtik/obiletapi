﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.WebUI.Models.VM
{
    public class SelectItemListVM
    {
        public string Text { get; set; }
        public int Value { get; set; }
    }
}
