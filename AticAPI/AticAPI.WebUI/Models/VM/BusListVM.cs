﻿namespace AticAPI.WebUI.Models.VM
{
    public class BusListVM
    {
        public int originId { get; set; }
        public int destinationId { get; set; }
        public DateTime date { get; set; }
    }
}
