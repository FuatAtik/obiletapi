﻿using AticAPI.Entities.Entities.Main;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AticAPI.WebUI.Models.VM
{
    public class BusJourneyVM
    {
        public List<BusJourneyData> busJourneyDatas { get; set; }
    }
}
