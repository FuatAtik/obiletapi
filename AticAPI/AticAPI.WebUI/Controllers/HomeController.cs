﻿using AticAPI.BusinessLayer.Abstract;
using AticAPI.BusinessLayer.Contracts;
using AticAPI.Entities.Entities.Core;
using AticAPI.Entities.Entities.Main;
using AticAPI.WebUI.Models;
using AticAPI.WebUI.Models.VM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AticAPI.WebUI.Controllers
{
    public class HomeController : Controller
    {
        //private IRepositoryWrapper _repositoryWrapper;

        //public HomeController(IRepositoryWrapper repositoryWrapper)
        //{
        //    _repositoryWrapper = repositoryWrapper;
        //}

        private readonly ISessionService _sessionService;
        private readonly ILocationService _locationService;
        private readonly IJourneyService _journeyService;
        public HomeController(ISessionService sessionService, ILocationService locationService, IJourneyService journeyService)
        {
            _sessionService = sessionService;
            _locationService = locationService;
            _journeyService = journeyService;
        }


        public async Task<IActionResult> Index()
        {
            if (String.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "DeviceId")) && string.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "SessionId")))
            {
                var session = await _sessionService.GetSession();
                CurrentSession.Set(HttpContext.Session, "DeviceId", session.Data.DeviceId);
                CurrentSession.Set(HttpContext.Session, "SessionId", session.Data.SessionId);
            }

            var busLocations = _locationService.GetBusLocations(
                new BusLocation()
                {
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Data = null,
                    Date = DateTime.Now,
                    Language = "tr-TR"
                });

            SelectList busLocationList = new SelectList(busLocations.Result.Data, "Id", "Name");

            BusLocationVM busLocationVM = new BusLocationVM()
            {
                selectList = busLocationList
            };

            return View(busLocationVM);
        }

        [HttpPost]
        public async Task<IActionResult> BusList(BusListVM model)
        {
            var journeyList = await _journeyService.GetBusJourneys(
                new BusJourney()
                {
                    Data = new Data()
                    {
                        OriginId = model.originId,
                        DestinationId = model.destinationId,
                        DepartureDate = model.date
                    },
                    Date = DateTime.Now,
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Language = "tr-TR"
                });


            BusJourneyVM busJourneyViewModel = new BusJourneyVM()
            {
                busJourneyDatas = journeyList.Data
            };

            if (busJourneyViewModel.busJourneyDatas != null && busJourneyViewModel.busJourneyDatas.Count > 0)
            {

                TempData["Result"] = journeyList.Data.Count() + " Adet Bilet Bulundu";
                return View(busJourneyViewModel);
            }
            TempData["Result"] = "Bilet Bulunamadı";
            return RedirectToAction("Index");
        }
    }
}