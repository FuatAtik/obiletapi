﻿using AticAPI.BusinessLayer.Abstract;
using AticAPI.BusinessLayer.Contracts;
using AticAPI.Entities.Entities.Core;
using AticAPI.Entities.Entities.Main;
using AticAPI.WebUI.Models;
using AticAPI.WebUI.Models.VM;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Atic.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeController : Controller
    {
        private readonly ISessionService _sessionService;
        private readonly ILocationService _locationService;
        private readonly IJourneyService _journeyService;
        public HomeController(ISessionService sessionService, ILocationService locationService, IJourneyService journeyService)
        {
            _sessionService = sessionService;
            _locationService = locationService;
            _journeyService = journeyService;
        }

        [HttpGet(Name = "GetBusLocations")]
        public async Task<IActionResult> Index()
        {
            if (String.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "DeviceId")) && string.IsNullOrEmpty(CurrentSession.Get(HttpContext.Session, "SessionId")))
            {
                var session = await _sessionService.GetSession();

                CurrentSession.Set(HttpContext.Session, "DeviceId", session.Data.DeviceId);
                CurrentSession.Set(HttpContext.Session, "SessionId", session.Data.SessionId);
            }

            var busLocations = _locationService.GetBusLocations(
                new BusLocation()
                {
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Data = null,
                    Date = DateTime.Now,
                    Language = "tr-TR"
                });

            SelectList busLocationList = new SelectList(busLocations.Result.Data, "Id", "Name");

            BusLocationVM busLocationVM = new BusLocationVM()
            {
                selectList = busLocationList
            };

            return Ok(busLocationVM);
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> GetByParametersBusList(int originId, int destinationId, DateTime date)
        {
            var journeyList = await _journeyService.GetBusJourneys(
                new BusJourney()
                {
                    Data = new Data()
                    {
                        OriginId = originId,
                        DestinationId = destinationId,
                        DepartureDate = date
                    },
                    Date = DateTime.Now,
                    DeviceSession = new DeviceSession()
                    {
                        DeviceId = CurrentSession.Get(HttpContext.Session, "DeviceId"),
                        SessionId = CurrentSession.Get(HttpContext.Session, "SessionId")
                    },
                    Language = "tr-TR"
                });

            if (journeyList != null)
            {
                BusJourneyVM busJourneyViewModel = new BusJourneyVM()
                {
                    busJourneyDatas = journeyList.Data
                };
                return View(busJourneyViewModel);
            }

            return RedirectToAction("Index");
        }
    }
}
